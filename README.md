# Chat application using ActionCable WebSockets

## Abstract

Recently I've discovered *socket programing* and I find it really exciting !
[irikados tutorial](https://iridakos.com/tutorials/2019/04/04/creating-chat-application-rails-websockets.html) 
guided me through the discovery of ActionCable, WebSockets, and helped me understanding 
some Rails underlying mechanisms.

# Notes

## Basic Devise set (with an extra attribute to User, username)

```ruby
gem 'devise'
```

```
bundle
rails generate devise:install
rails generate devise User username:string
```

```ruby
#db/migrate/<devise_migration>
# username should be uniquue and not null
add_index :users, :username, unique: true
t.string :username, null: false

#app/models/user.rb
# add the corresponding valition rule
validates :username, uniqueness: true, presence: true
```

##  Inverse of

Memory optimization + allows associated objects (`has_many :through`, 
`accpets_nested_attributes_for` in a `:has_many`) to be saved correctly.

Since version 4.1, Rails try to automaticaly set the :inverse_of option

## room_controller#show

```ruby
#app/controllers/room_controller.rb
# RoomMessage object will be used in the view form to create a message
# @room_messages will be user to access to the user gravatar.
# .includes is incredible

def show
  @room          = Room.find(params[:id]) # from :load_entities
  @room_message  = RoomMessage.new room: @room
  @room_messages = @room.room_messages.includes(:user)
end
```

## N+1 problem

great article: https://medium.com/@bretdoucette/n-1-queries-and-how-to-avoid-them-a12f02345be5
great example (with only 2 users...) :

```ruby
# rails console
r = Room.last

# N+1 => 1 query per user
r.room_messages.all.map(&:user)
  RoomMessage Load (0.2ms)  SELECT "room_messages".* FROM "room_messages" WHERE "room_messages"."room_id" = ?  [["room_id", 1]]
  User Load (0.0ms)  SELECT  "users".* FROM "users" WHERE "users"."id" = ? LIMIT ?  [["id", 1], ["LIMIT", 1]]
  User Load (0.0ms)  SELECT  "users".* FROM "users" WHERE "users"."id" = ? LIMIT ?  [["id", 2], ["LIMIT", 1]]
=> [#<User id: 1, email: "foo@bar.com", username: "foobar", created_at: "2019-08-01 14:20:28", updated_at: "2019-08-01 14:20:28">, #<User id: 2, email: "gga@azeaz.com", username: "barz", created_at: "2019-08-01 14:30:41", updated_at: "2019-08-01 14:30:41">]


# .includes, 1 query for all users
r.room_messages.includes(:user).all.map(&:user)
  RoomMessage Load (0.1ms)  SELECT "room_messages".* FROM "room_messages" WHERE "room_messages"."room_id" = ?  [["room_id", 1]]
  User Load (0.2ms)  SELECT "users".* FROM "users" WHERE "users"."id" IN (?, ?)  [["id", 1], ["id", 2]]
=> [#<User id: 1, email: "foo@bar.com", username: "foobar", created_at: "2019-08-01 14:20:28", updated_at: "2019-08-01 14:20:28">, #<User id: 2, email: "gga@azeaz.com", username: "barz", created_at: "2019-08-01 14:30:41", updated_at: "2019-08-01 14:30:41">]
```

## Redis

DB where records lives in memory. Could also be used as cache or message broker.
```
sudo apt instal redis-server
```

```ruby
#Gemfile
gem 'redis'


#config/cable.yml

development:
  adapter: redis
  url: <%= ENV.fetch("REDIS_URL") { "redis://localhost:6379/1" } %>
  channel_prefix: rails-chat-tutorial_development

test:
  adapter: async

production:
  adapter: redis
  url: <%= ENV.fetch("REDIS_URL") { "redis://localhost:6379/1" } %>
  channel_prefix: rails-chat-tutorial_production
```

`channel_prefix`: 
> Additionally, a channel_prefix may be provided to avoid channel name collisions when using the same Redis server for multiple applications

## Warden Hooks

see [/config/initializers/warden_hooks.rb](https://gitlab.com/kawsay/rails-actioncable-chat/blob/master/config/initializers/warden_hooks.rb)

## ActiveCable Connection

see [/app/channels/application_cable/connection.rb](https://gitlab.com/kawsay/rails-actioncable-chat/blob/master/app/channels/application_cable/connection.rb)

## ActiveCable Channel

see [/app/channels/room_channel.rb](https://gitlab.com/kawsay/rails-actioncable-chat/blob/master/app/channels/room_channel.rb)


