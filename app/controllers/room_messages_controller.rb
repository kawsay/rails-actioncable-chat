class RoomMessagesController < ApplicationController
  before_action :load_entities

  def create
    @room_message = RoomMessage.create(user: current_user,
                                       room: @room,
                                       message: params.dig(:room_message, :message))
                                       # dig replace params[:room_message][:message], return nil if nothing is found
                                       # https://tiagoamaro.com.br/2016/08/27/ruby-2-3-dig/
    RoomChannel.broadcast_to @room, @room_message
  end

  private

  def load_entities
    @room = Room.find params.dig(:room_message, :room_id)
  end
end
