# Depending on the room the user's in, we'll set the stream from
# which data will be sent back and forth.
#
# `stream_for` generates a stream name for the given object (here, an
# instance of Room).
# We can then broadcast like so:
#     RoomChannel.broadcast_to(room_object, data)
# see: https://guides.rubyonrails.org/action_cable_overview.html#streams
#
#
#
# A Channel provides the basic structure of grouping behavior into
# logical units when using WebSockets. Similar to what a controller
# does in a regular MVC setup.
#
# A channel object will be created when the cable consumer becomes a suscriber,
# and is destroyed when consumer disconnect.
#
# A "consumer" is, in term of ActionCable, the client of a WebSocket.
#


class RoomChannel < ApplicationCable::Channel
  def subscribed
    room = Room.find(params[:room])
    stream_for room
  end
end

