# "Connections from the foundation of the client-server relationship"
# 
# Each time a WebSocket is accepted, a Connection object is instantiated,
# and is the parent of all subscriptions that are created from there on.
#
# A user will create 1 consumer-connection per tab, window, or device.
#
#
# `find_verified_user` refers to the cookie set in config/initializers/warden_hook.rb
#


module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :current_user

   def connect
     self.current_user = find_verified_user
   end

   private

   def find_verified_user
     if verified_user = User.find_by(id: cookies.signed['user.id'])
       verified_user
     else
       reject_unauthorized_connection
     end
   end
  end
end
