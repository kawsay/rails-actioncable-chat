# WebSockets can't access the user session but do have access to cookies.
# We're setting a cookie when user log in and deleting it when he logs out.
#
#
# As far as I understand it:
#   - Rack 
#     -> is the middleware (interface) between the Web Server (Puma) and 
#        the Web Framework (Ror)
#     -> responsible of the interoperability between those 2 components,
#        which can individualy be substituted by any solution
#            Puma -> Unicorn, Think...
#            RoR  -> Sinatra, Padrino...
#     -> main tasks : Loggin, Exception handling, Serving static files,
#                     Access control, Profiling
#     -> `rails middleware`: list all middlewares used in the current
#        application in the order an incoming request will meet them
#        ==> https://www.youtube.com/watch?v=7u5TvA_l1wo
#
#   - Warden
#     -> a Rack-based middleware which provides authentification mechanisms
#     -> used by Devise
#
# +---------------+
# |  Web Browser  | -> Mozilla, ...
# +---------------+
# |  Web Server   | -> Puma, Unicorn, ...
# +---------------+
# |  Middleware   | -> Rack
# +---------------+
# | Web Framework | -> RoR, Sinatra, ...
# +---------------+
#

Warden::Manager.after_set_user do |user, auth, opts|
  scope = opts[:scope]
  auth.cookies.signed["#{scope}.id"] = user.id
end

Warden::Manager.before_logout do |user, auth, opts|
  scope = opts[:scope]
  auth.cookies.signed["#{scope}.id"] = nil
end

